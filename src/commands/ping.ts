import { ICommand } from 'wokcommands';

export default {
	category: 'Information',
	description: 'Get the bot ping.',

	slash: true,
	testOnly: true,

	callback: ({ interaction }) => {

		const choices = ['Is this really my ping?', 'Is this okay? I can\'t look!', 'I hope it isn\'t bad!'];
		const response = choices[Math.floor(Math.random() * choices.length)];

		interaction.reply({
			content: `Oi! My ping is ${interaction.client.ws.ping}ms. *${response}*`,
			ephemeral: true,
		})


		
	}
} as ICommand;