import { ICommand } from 'wokcommands';

export default {
	category: 'Information',
	description: 'Get the bot uptime.',

	slash: true,
	testOnly: true,

	callback: ({ interaction }) => {

		function getUptime(): string {
			const uptime: Date = new Date(interaction.client.uptime!);

			let returnValue: string = 'Bot not online.';

			if (uptime.getSeconds() > 60) {
				returnValue = `${uptime.getMinutes()} minutes and ${uptime.getSeconds()} ${uptime.getSeconds() > 1 ? 'seconds' : 'second'}`;

				if (uptime.getMinutes() > 60) {
					returnValue = `${uptime.getHours()} hours, ${uptime.getMinutes()} minutes and ${uptime.getSeconds()} ${uptime.getSeconds() > 1 ? 'seconds' : 'second'}.`;
				}
			}

			returnValue = `${uptime.getSeconds()} ${uptime.getSeconds() > 1 ? 'seconds' : 'second'}`;

			return returnValue;
		}


		


		interaction.reply({
			content: `Oi! My uptime is ${getUptime()}`,
			ephemeral: true,
		})

		
	}
} as ICommand;