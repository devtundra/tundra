import dotenv from 'dotenv'

class Config {
	public static get(key: string): string {
		dotenv.config()
		return process.env[key.toUpperCase()]!
	}
}


export default Config;