import { Client, Intents } from 'discord.js'
import WOKCommands from 'wokcommands'
import path from 'path'
import Config from './helpers/config'

const client: Client = new Client({
	intents: [
		Intents.FLAGS.GUILDS,
		Intents.FLAGS.GUILD_MESSAGES,
	]
})

client.once('ready', () => {
	new WOKCommands(client, {
		commandsDir: path.join(__dirname, 'commands'),
		testServers: ['761834124448497684'],
		typeScript: true,
	})
})

client.on('messageCreate', (message) => {
	if (message.content === 'ping') {
		message.reply({
			content: 'pong'
		})
	}
})

client.login(Config.get('token'))